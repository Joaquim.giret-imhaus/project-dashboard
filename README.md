# Dashboard project

This project is a dashboard made in React.js to visualize datas on a mongoDB database using a RESTful API developped by Joaquim Giret-Imhaus & Malo Du Breil De Pontbriand

The API repo is [here](https://gitlab.com/Joaquim.giret-imhaus/dashboard-project-api-rest)

The dashboard contains 4 sections :

* Miscellaneaous : contains widgets made before the kind of data (Music) to use was given to us, so not linked to our database but some fancy widgets

* Music : displays all the different albums contained in our database in an Itunes/Spotify-like way

* Datas : contains 6 dynamic widgets, corresponding with the database through our API such as RadarCharts, BarCharts, Timeline, ...

* Admin : contains a fancy form with fieldDecorators and fields rules which let you add new artists to the database and a list you can refresh containing all artists to see your new insertion

## Dependencies

Major dependecies are : 
* Antd
* Lodash
* Recharts
* @Material-Ui
* Axios
* React-router

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.