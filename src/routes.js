import React from 'react';
import {Route, Switch} from 'react-router-dom';
import Admin from './containers/Admin';
import Miscellaneous from './containers/Miscellaneous';
import Music from './containers/Music';
import MusicLibrary from './containers/MusicLibrary'


const routes = () => {
  return (
    <Switch>
    <Route path="/" exact component={Miscellaneous}/>
    <Route path="/music" exact component={MusicLibrary}/>
    <Route path="/data" exact component={Music}/>
    <Route path="/admin" exact component={Admin} />
  </Switch>
  );
};
export default routes;