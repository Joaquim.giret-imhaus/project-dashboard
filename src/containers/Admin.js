import React, { Component } from 'react';

import MyForm from '../components/MyForm';
import {Card, BackTop, Row, Col} from 'antd';
import AdminList from '../components/AdminList';


class Admin extends Component {
    
    render() {

        const homeColor = {
            "backgroundColor": "#CDCDCD"
        }

        const rowStyle = {
            "paddingTop": "25px",
            "paddingBottom": "25px"
        }

        return (
            <div style={homeColor}>
                <BackTop/>
                <Row type="flex" justify="center" style={rowStyle}>
                    <Col span={1}/>
                    <Col span={22}>
                        <Card style={{"borderRadius": "7px"}}>
                            <MyForm/>
                        </Card>
                    </Col>
                    <Col span={1}/>
                </Row>

                <Row type="flex" justify="center" style={rowStyle}>
                    <Col span={1}/>
                    <Col span={22}>
                        <Card style={{"borderRadius": "7px"}}>
                            <AdminList/>
                        </Card>
                    </Col>
                    <Col span={1}/>
                </Row>
            </div>
        );
    }
}

export default Admin;