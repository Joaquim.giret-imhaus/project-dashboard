import React, { Component } from 'react';
import MyTimeline from "../components/MyTimeline";
import MyProgression from '../components/MyProgression';
import MyToDoList from '../components/MyToDoList';
import MyLineChart from '../components/MyLineChart';
import MyWeatherWidget from '../components/MyWeatherWidget';
import { Row, Col, BackTop } from 'antd';

import MyHorizontalP from '../components/MyHorizontalP';

class Miscellaneous extends Component {
    render() {
        const rowStyle = {
            "paddingTop": "25px",
            "paddingBottom": "25px"
        }

        const homeColor = {
            "backgroundColor": "#CDCDCD"
        }

        return (
            <div style={homeColor}>
                <BackTop/>
                <Row type="flex" justify="center" style={rowStyle}>
                    <Col xs={{ span: 1}} lg={{span: 1}}/>
                    <Col xs={{ span: 7}} lg={{span: 7}}>
                        <MyHorizontalP/>
                    </Col>
                    <Col xs={{ span: 1}} lg={{span: 1}}/>
                    <Col xs={{ span: 14}} lg={{span: 14}}>
                        <MyLineChart/>
                    </Col>
                    <Col xs={{ span: 1}} lg={{span: 1}}/>
                </Row>

                <Row type="flex" justify="center" style={rowStyle}>
                    <Col xs={{ span: 8}} lg={{span: 8}}>
                        <MyWeatherWidget/>
                    </Col>
                    <Col xs={{ span: 1}} lg={{span: 1}}/>
                    <Col xs={{ span: 13}} lg={{span: 13}} >
                        <MyProgression/>
                        <br/>
                        <MyTimeline/>
                    </Col>
                </Row>

                <Row type="flex" justify="center" style={rowStyle}>
                    <Col xs={{ span: 20}} lg={{span: 20}} >
                        <MyToDoList/>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default Miscellaneous;
