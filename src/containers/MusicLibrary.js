import React, { Component } from 'react';
import MusicLibraryItem from '../components/MusicLibraryItem';
import {BackTop} from 'antd';
import _ from 'lodash';

import axios from 'axios';

class MusicLibrary extends Component {
    state = {
        musicsList: [],
    }

    componentWillMount(){
        this.fetchMusicList();
    }

    fetchMusicList(){
        axios.get(`http://localhost:3000/albums`)
        .then(({ data }) => {
          this.setState({musicsList: data});
        })
    }

    renderMusicItem = (musicItem, id) => {
        return <MusicLibraryItem key={id} musicItem={musicItem}/>
    }


    render() {
        const { musicsList } = this.state;
        console.log("musicsList", musicsList);

        return (
            <div style={{"backgroundColor": "#CDCDCD", display: "inline-flex", flexWrap: "wrap"}}>
            <BackTop/>
                {musicsList.length && _.map(musicsList, this.renderMusicItem) }
            </div>
        );
    }
}

export default MusicLibrary;