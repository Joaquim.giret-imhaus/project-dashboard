import React, { Component } from 'react';
import { Row, Col, BackTop } from 'antd';
import MyRadarChart from '../components/MyRadarChart';
import Top5 from '../components/Top5';
import MyBarChart from '../components/MyBarChart';
import MyEventTimeline from '../components/MyEventTimeline';
import Birthday from '../components/Birthday';

class Music extends Component {
    render() {

        const homeColor = {
            "backgroundColor": "#CDCDCD"
        }

        const rowStyle = {
            "paddingTop": "25px",
            "paddingBottom": "25px"
        }

        return (
            <div style={homeColor}>
                <BackTop/>
                <Row type="flex" justify="center" style={rowStyle}>
                    <Col span={1}/>
                    <Col span={7}>
                        <MyRadarChart/>
                    </Col>
                    <Col span={1}/>
                    <Col span={14}>
                        <Top5/>
                    </Col>
                    <Col span={1}/>
                </Row>

                <Row type="flex" justify="center" style={rowStyle}>
                    <Col span={2}/>
                    <Col span={20}>
                        <MyBarChart/>
                    </Col>
                    <Col span={2}/>
                </Row>

                <Row type="flex" justify="left" style={rowStyle}>
                    <Col span={2}/>
                    <Col span={8} >
                     <MyEventTimeline/>
                    </Col>
                    <Col span={2}/>
                    <Col span={10}>
                    <Birthday/>
                    </Col>
                    <Col span={2}/>
                </Row>
                
            </div>
        );
    }
}

export default Music;