import React, { Component } from 'react';
import { Card } from 'antd';


class MusicLibraryItem extends Component {
        render() {
            const {AlbumName, AlbumCover, AlbumDate} = this.props.musicItem;
            return (
                <Card style={{"backgroundColor": "#edeaea", width: 300, height: 315, margin: "15px", "border-radius": "10px"}}>

                    <img src={AlbumCover} alt="albumCover" width={250} height={250}/>
                    <p style={{marginBottom: 0}}>{AlbumName}</p>({AlbumDate})
                </Card>
            );
        }
}

export default MusicLibraryItem;