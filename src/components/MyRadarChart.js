import React, { Component } from 'react';
import axios from 'axios';
import {Card} from 'antd'

import {RadarChart, PolarGrid, PolarAngleAxis, PolarRadiusAxis, Radar, Legend} from 'recharts';

class MyRadarChart extends Component {

    state = {
        genresList: [],
    }

    componentWillMount(){
        this.fetchGenres();
    }

    fetchGenres(){
        axios.get(`http://localhost:3000/albums/genres`)
        .then(({ data }) => {
            console.log("genreslist", data);
          this.setState({genresList: data});
        })
    }

    render() {
        const {genresList}=this.state;

        return (
            <Card style={{"borderRadius": "7px"}}>
                <RadarChart outerRadius={90} width={320} height={250} data={genresList}>
                    <PolarGrid />
                    <PolarAngleAxis dataKey="AlbumGenre" />
                    <PolarRadiusAxis angle={90} domain={[0, 4]} />
                    <Radar name="Genres of Albums owned by our Record" dataKey="count" stroke="#8884d8" fill="#8884d8" fillOpacity={0.6} />
                    <Legend />
                </RadarChart>
            </Card>
        );
    }
}

export default MyRadarChart;


