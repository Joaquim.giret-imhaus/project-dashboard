import React, { Component } from 'react';

import axios from 'axios';



import { Form, Icon, Input, Button } from 'antd';
const FormItem = Form.Item;

class HorizontalLoginForm extends Component {
   handleSubmit(e) {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
        axios.put('http://localhost:3000/artists/', values);
      }
    });
    console.log(e.target.value);
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <React.Fragment>
        <h2>Form to add a new artist</h2>
        <Form inline onSubmit={this.handleSubmit.bind(this)}>
          <FormItem>
            {getFieldDecorator('Name', {
              rules: [{ required: true, message: 'Please input the name of the artist you are trying to add!' }],
            })(
              <Input addonBefore={<Icon type="user" />} placeholder="Name of the artist" />
            )}
          </FormItem>


          <FormItem>
            {getFieldDecorator('Birthday', {
              rules: [{ required: true, message: 'Please input the birthday of the artist you are trying to add!' }],
            })(
              <Input addonBefore={<Icon type="calendar" />} placeholder="Birthday" />
            )}
          </FormItem>

          <FormItem>
            {getFieldDecorator('Followers', {
              rules: [{ required: true, message: 'Please input the number of followers of the artist you are trying to add!' }],
            })(
              <Input addonBefore={<Icon type="heart" />} placeholder="Followers" />
            )}
          </FormItem>

          <FormItem>
            {getFieldDecorator('Album', {
              rules: [{ message: 'Please input the album of the artist you are trying to add!' }],
            })(
              <Input addonBefore={<Icon type="file-add" />} placeholder="Album" />
            )}
          </FormItem>


          <FormItem>
            <Button type="primary" htmlType="submit">Add a new Artist</Button>
          </FormItem>
        </Form>
      </React.Fragment>
    )
    }
}

HorizontalLoginForm = Form.create({ name: 'horizontal_login' })(HorizontalLoginForm);
export default HorizontalLoginForm;