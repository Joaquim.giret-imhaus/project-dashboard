import React, { Component } from 'react';
import _ from 'lodash';
import axios from 'axios';
import {Card} from 'antd';

class Top5 extends Component {

    state = {
        top5List: [],
    }

    componentWillMount(){
        this.fetchGenres();
    }

    fetchGenres(){
        axios.get(`http://localhost:3000/artists/followers`)
        .then(({ data }) => {
            console.log("List", data);
            const top5 = _.slice(data, 0, 5);
            console.log("top5List", top5);
          this.setState({top5List: top5});
        })
    }

    renderTop5 = (item, index) => {
        return (
            <p>
                {index +1}. {item.Name} - {item.Followers} followers
            </p>
        )
    }

    render() {
        const {top5List}=this.state;
        return (
            <Card style={{"borderRadius": "7px"}}>
                <p><h1>Artistes les plus suivis sur la plateforme</h1></p>
                {top5List.length && _.map(top5List, this.renderTop5)}
            </Card>
        );
    }
}

export default Top5;