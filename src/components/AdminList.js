import React, { Component } from 'react';
import { Table, Button } from 'antd';
import axios from 'axios';

const columns = [{
  title: 'Name',
  dataIndex: 'Name',
  key: 'Name',
}, {
  title: 'Birthday',
  dataIndex: 'Birthday',
  key: 'Birthday',
}, {
  title: 'Followers',
  dataIndex: 'Followers',
  key: 'Followers',
},{
  title: 'Albums',
  dataIndex: 'Album',
  key: 'Album',
  render: albums => (
    <span>
      {albums.map(album => {
        return `${album}, ` ;
      })}
    </span>
  )
}];



class AdminList extends Component {
    state ={
        artistsList : []
    }

    componentDidMount(){
        this.fetchData();
    }

    fetchData(){
      axios.get('http://localhost:3000/artists/')
        .then(({ data }) => {
            console.log("ArtistList", data);
          this.setState({artistsList: data});
        })
    }

    render() {
        return (
          <React.Fragment>
            <h2>List of artists</h2>
            <Table columns={columns} dataSource={this.state.artistsList} />
            <Button type="primary" onClick={this.fetchData.bind(this)}>Refresh Table</Button>
          </React.Fragment>
        );
    }
}

export default AdminList;