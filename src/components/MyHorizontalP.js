import React, { Component } from 'react';
import { Card, Progress } from 'antd';

class MyHorizontalP extends Component {
    constructor(props){
        super(props);
        this.state = {
            balancePercent: 67,
            percent: 52,
            foodPercent: 35,
            japPercent: 100,
            livretPercent: 45
        }
    }
    render() {
        return (
            <div>
                <Card style={{"borderRadius": "7px"}}>
                        Income this month:
                    <Progress percent={this.state.balancePercent} format={()=>(`${this.state.balancePercent} €`)} />

                        Spent on food this month :
                    <Progress percent={this.state.foodPercent} format={()=>(`${this.state.foodPercent} €`)} />

                        Spent on All-you-can-eat Japanese Restaurant this month :
                    <Progress percent={this.state.japPercent} format={()=>(`${this.state.japPercent} €`)} />

                        Spent on Hobbies this month :
                    <Progress percent={this.state.percent} format={()=>(`${this.state.percent} €`)} />

                        Balance Livret Jeune :
                    <Progress percent={this.state.livretPercent} format={()=>(`${this.state.livretPercent} €`)} />
                </Card>
            </div>
        );
    }
}

export default MyHorizontalP;