import React, { Component } from 'react';
import ReactWeather from 'react-open-weather';
import 'react-open-weather/lib/css/ReactWeather.css';
import {Input } from 'antd';

class MyWeatherWidget extends Component {
    constructor(props){
        super(props);
        this.state = {
            fired: false,
            cityName: ""
        }

        this.cityNameInput = this.cityNameInput.bind(this);
        this.fireHandler = this.fireHandler.bind(this);

    }

    cityNameInput(event){
        console.log("This has entered", "This is the state value", this.state.cityName, "this is the event ", event.target.value);
        this.setState({cityName: event.target.value, fired:false})
    }

    fireHandler(event){
        this.setState({fired: true})
    }

    render() {
        const {cityName, fired} = this.state;
        return (
            <div>
                <Input
                    placeholder="Enter city name"
                    value={cityName}
                    onChange={(event)=>this.cityNameInput(event)}
                    onPressEnter={(event)=>this.fireHandler(event)}
                />

                {
                    fired && (
                        <ReactWeather
                        forecast="today"
                        apikey="0796298a9ed4435b835201717191202"
                        type="city"
                        city={cityName}/>
                    )
                }
            </div>
        );
    }
}

export default MyWeatherWidget;