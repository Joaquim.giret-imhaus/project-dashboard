import React, { Component } from 'react';
import { Card } from 'antd';
//import { LineChart, Line, CartesianGrid, XAxis, YAxis, Tooltip, ResponsiveContainer } from 'recharts';
//import { BarChart, Bar, XAxis, YAxis, Tooltip, Legend,} from 'recharts';
var LineChart = require("react-chartjs").Line;

class MyLineChart extends Component {
/*     render() {
        const data = [{name: 'Janvier', uv: 400, pv: 2000, amt: 2400},
{name: 'Fevrier', uv: 800, pv: 2400, amt: 2000},
{name: 'Mars', uv: 500, pv: 2200, amt: 2100},
{name: 'Avril', uv: 600, pv: 2300, amt: 2300}];
        return (
            <Card>
                <ResponsiveContainer width='100%' height={300}>
                <LineChart width={600} height={300} data={data} margin={{ top: 5, right: 20, bottom: 5, left: 0 }}>
                    <Line type="monotone" dataKey="uv" stroke="#8884d8" />
                    <Line type="monotone" dataKey="pv" stroke="#8774d8" />
                    <Line type="monotone" dataKey="amt" stroke="#8664d8" />
                    <CartesianGrid stroke="#ccc" strokeDasharray="5 5" />
                    <XAxis dataKey="name" />
                    <YAxis />
                    <Tooltip />
                </LineChart>
                </ResponsiveContainer>
            </Card>
        );
    } */

    /* data = [
        { Year: "1929", BMW: "1889", Toyota: "9547", Mercedes: "4881"},
        { BMW: "6548", Mercedes: "8096", Toyota: "4741", Year: "1930"},
        { Year: "1931", BMW: "5013", Toyota: "6269", Mercedes: "3908"},
        { Year: "1932", BMW: "2468", Toyota: "9858", Mercedes: "1623"},
        { Year: "1933", BMW: "3364", Toyota: "5595", Mercedes: "8638"},
        { Year: "1934", BMW: "2032", Toyota: "2570", Mercedes: "8041"}
    ];

    render(){
        return (
            <BarChart
                width={600}
                height={300}
                data={this.data}
                margin={{top: 5, right: 30, left: 20, bottom: 5}}>
                <XAxis type="category" dataKey="Year"/>
                <YAxis type="number"/>
                <Tooltip cursor={false}/>
                <Legend />
                <Bar dataKey="BMW" stroke="#8884d8" fill="#8884d8" background={{ stroke: '#eee' }} isAnimationActive={true} barSize={20}/>
                <Bar dataKey="Mercedes" stroke="#82ca9d" fill="#82ca9d" background={{ stroke: '#eee' }} isAnimationActive={false} barSize={30}/>
            </BarChart>
        )
    } */


    render(){
        const chartData = {
            labels: ["January", "February", "March", "April", "May", "June", "July"],
            datasets: [
                {
                    label: "My First dataset",
                    fillColor: "rgba(220,220,220,0.2)",
                    strokeColor: "rgba(220,220,220,1)",
                    pointColor: "rgba(220,220,220,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(220,220,220,1)",
                    data: [65, 59, 80, 81, 56, 55, 40]
                },
                {
                    label: "My Second dataset",
                    fillColor: "rgba(151,187,205,0.2)",
                    strokeColor: "rgba(151,187,205,1)",
                    pointColor: "rgba(151,187,205,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: [28, 48, 40, 19, 86, 27, 90]
                }
            ]
        };

        const chartOptions = {

            ///Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines : true,
        
            //String - Colour of the grid lines
            scaleGridLineColor : "rgba(0,0,0,.05)",
        
            //Number - Width of the grid lines
            scaleGridLineWidth : 1,
        
            //Boolean - Whether to show horizontal lines (except X axis)
            scaleShowHorizontalLines: true,
        
            //Boolean - Whether to show vertical lines (except Y axis)
            scaleShowVerticalLines: true,
        
            //Boolean - Whether the line is curved between points
            bezierCurve : true,
        
            //Number - Tension of the bezier curve between points
            bezierCurveTension : 0.4,
        
            //Boolean - Whether to show a dot for each point
            pointDot : true,
        
            //Number - Radius of each point dot in pixels
            pointDotRadius : 4,
        
            //Number - Pixel width of point dot stroke
            pointDotStrokeWidth : 1,
        
            //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
            pointHitDetectionRadius : 20,
        
            //Boolean - Whether to show a stroke for datasets
            datasetStroke : true,
        
            //Number - Pixel width of dataset stroke
            datasetStrokeWidth : 2,
        
            //Boolean - Whether to fill the dataset with a colour
            datasetFill : true,

            //String - A legend template
            legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"><%if(datasets[i].label){%><%=datasets[i].label%><%}%></span></li><%}%></ul>",
        
            //Boolean - Whether to horizontally center the label and point dot inside the grid
            offsetGridLines : false
        };
        return(
            <Card style={{"borderRadius": "7px"}}>
                <LineChart data={chartData} options={chartOptions} width="600" height="250"/>
            </Card>
        )
    }

}

export default MyLineChart;