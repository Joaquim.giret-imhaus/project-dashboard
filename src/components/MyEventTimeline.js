import React, { Component } from 'react';
import { Timeline, Card } from 'antd';
import axios from 'axios';
import _ from 'lodash';



class MyEventTimeline extends Component {
    state = {
        eventsList: [],
    }

    componentWillMount(){
        this.fetchEventList();
    }

    fetchEventList(){
        axios.get(`http://localhost:3000/event/date/`)
        .then(({ data }) => {
          this.setState({eventsList: data});
        })
    }

    renderEventItem = (eventItem, id) => {
        return <Timeline.Item key={id}>{eventItem.EventDate} - {eventItem.EventType} avec <bold style={{'color':'#5c98f9'}}>{eventItem.EventArtiste}</bold> </Timeline.Item>
    }
    render() {
        const { eventsList } = this.state;

        return (
            <Card style={{"borderRadius": "7px" , "height":'250px'}}>
            <h2>Evénements à venir</h2><br/>
                <Timeline>
                {eventsList.length && _.map(eventsList, this.renderEventItem) }
                </Timeline>
            </Card>
    
           
        );
    }
}

export default MyEventTimeline;