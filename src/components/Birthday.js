import React, { Component } from 'react';
import axios from 'axios';
import {Card} from 'antd';

class Birthday extends Component {

    state = {
        birthday: [],
    }

    componentWillMount(){
        this.fetchBirthday();
    }

    fetchBirthday(){
        axios.get(`http://localhost:3000/artists/nextBirthday`)
        .then(({ data }) => {
            console.log("List", data);
            this.setState({birthday: data});
        })
    }
    render() {
        const {birthday}=this.state;
        return (
            <Card style={{"borderRadius": "7px"}}>
                <h1>It will be {birthday.Name}'s Birthday</h1>
                <h2>{birthday.nextBirthday}</h2>
            </Card>
        );
    }
}

export default Birthday;