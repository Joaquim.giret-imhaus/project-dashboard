import React, { Component } from 'react';
import {Card, Progress} from 'antd';

class MyProgression extends Component {
    render() {
        return (
            <Card style={{"borderRadius": "7px"}}>
                <p>Avancement de la collecte des fonds pour le cadeau de Loïc</p>
                <Progress type="dashboard" percent={75} />
            </Card>
        );
    }
}

export default MyProgression;