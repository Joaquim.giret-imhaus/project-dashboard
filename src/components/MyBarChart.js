import React, { PureComponent } from 'react';
import {
  BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
} from 'recharts';
import axios from 'axios'; 
import {Card} from 'antd'




export default class MyBarChart extends PureComponent {
  static jsfiddleUrl = 'https://jsfiddle.net/alidingling/30763kr7/';

  state={
    MusicStreamList :[],
  }

  componentWillMount(){
    this.fetchStreams();
}

fetchStreams(){
    axios.get(`http://localhost:3000/musics/streams`)
    .then(({ data }) => {
        console.log("MusicStream", data);
      this.setState({MusicStreamList: data});
    })
}
  render() {
    const {MusicStreamList}=this.state;

    return (
      <Card style={{"borderRadius": "7px"}}>
      <BarChart
        width={1200}
        height={300}
        data={MusicStreamList}
        margin={{
          top: 5, right: 20, left: 20, bottom: 5,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="MusicTitle" />
        <YAxis/>
        <Tooltip />
        <Legend />
        <Bar dataKey="MusicStream" fill="#8884d8" />
      </BarChart>
      </Card>
    );
  }
}
