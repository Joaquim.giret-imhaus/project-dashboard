import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ExploreIcon from '@material-ui/icons/Explore';
import LibraryMusicIcon from '@material-ui/icons/LibraryMusic';
import DataUsageIcon from '@material-ui/icons/DataUsage';
import HowToRegIcon from '@material-ui/icons/HowToReg';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import {Link} from 'react-router-dom';

const stylesAppBar = {
    root: {
      flexGrow: 1,
    },
    grow: {
      flexGrow: 1,
    },
    menuButton: {
      marginLeft: -12,
      marginRight: 20,
    },
    list: {
        width: 250,
      }
  };

class SwipeableTemporaryDrawer extends React.Component {
  state = {
    left: false,
    right: false,
  };

  toggleDrawer = (side, open) => () => {
    this.setState({
      [side]: open,
    });
  };

  render() {
    const { classes } = this.props;

    const sideList = (
      <div className={classes.list}>
        <List>
            <ListItem button key="Miscellaneous">
              <ListItemIcon><ExploreIcon /></ListItemIcon>
              <Link to="/">Miscellaneous</Link>
            </ListItem>

            <ListItem button key="Music Library">
              <ListItemIcon><LibraryMusicIcon /></ListItemIcon>
              <Link to="/music">Music Library</Link>
            </ListItem>

            <ListItem button key="Datas">
              <ListItemIcon><DataUsageIcon /></ListItemIcon>
              <Link to="/data">Datas</Link>
            </ListItem>
        </List>
        <Divider />
        <List>
          <ListItem button key="Admin">
              <ListItemIcon><HowToRegIcon /></ListItemIcon>
              <Link to="/admin">Admin</Link>
            </ListItem>
        </List>
      </div>
    );

    return (
      <div>

       <AppBar position="static">
        <Toolbar>
          <IconButton className={classes.menuButton} color="inherit" aria-label="Menu" onClick={this.toggleDrawer('left', true)}>
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" color="inherit">
            Dashboard
          </Typography>
        </Toolbar>
      </AppBar>

        <SwipeableDrawer
          open={this.state.left}
          onClose={this.toggleDrawer('left', false)}
          onOpen={this.toggleDrawer('left', true)}
        >
          <div
            tabIndex={0}
            role="button"
            onClick={this.toggleDrawer('left', false)}
            onKeyDown={this.toggleDrawer('left', false)}
          >
            {sideList}
          </div>
        </SwipeableDrawer>
      </div>
    );
  }
}

SwipeableTemporaryDrawer.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(stylesAppBar)(SwipeableTemporaryDrawer);