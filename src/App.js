import React, { Component } from 'react';
import SwipeableTemporaryDrawer from './components/SwipeableTemporaryDrawer'
import './App.css';

import Routes from "./routes";


class App extends Component {
  render() {
    return (
      <div className="App">
        <SwipeableTemporaryDrawer/>
        <Routes/>
        <footer style={{backgroundColor: "#3f51b5", 'color':'#ffffff'}}>
          Made and designed by Joaquim and Malo
        </footer>
      </div>
    );
  }
}

export default App;
